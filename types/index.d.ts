import Vue, { PluginObject } from 'vue';

export declare const Plugin: PluginObject<PluginOptions>;

export interface PluginOptions {
  [key: string]: unknown;
}

export * from './modal';

export default Plugin;
