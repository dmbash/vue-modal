import Vue from 'vue';

export interface ComponentProps {
  [key: string]: unknown;
}

export interface ModalProps {
  component: Vue;
  id: string;
  props: ComponentProps;
}

export interface Context {
  modalContainer: Vue;
}

export type Spawn = (component: Vue, componentProps: ComponentProps) => void;

export type Kill = (modalId: string) => void;

export type KillAll = () => void;

export type Show = (modalId: string) => void;

export type Hide = (modalId: string) => void;

export interface Modal {
  spawn: Spawn;
  kill: Kill;
  killAll: KillAll;
  show: Show;
  hide: Hide;
  ctx: Context;
}
